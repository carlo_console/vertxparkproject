var parkName = ["A1", "A2", "A3", "A4", "B1", "B2", "B3", "B4"];
var parkState = [];
var time;

function main()
{
	getCurrentDate();
	setInterval(updateTime, 1000);
	
	var init = false;
	var eb = new EventBus("http://192.168.43.50:8080/eventbus");
	
	eb.onopen = function ()
	{
		eb.registerHandler("init", function (err, msg){
			if (init == false)
			{
				parkState = [msg.body.A1, msg.body.A2, msg.body.A3, msg.body.A4,
							 msg.body.B1, msg.body.B2, msg.body.B3, msg.body.B4];
				
				for(var i = 0; i < parkName.length; i++)
					updateSlot(parkName[i], parkState[i]);
				
				updateLight(msg.body.LIGHT);
				
				eb.registerHandler("news-feed", function (err, msg) {
					if (msg.body.name == "LIGHT")
						updateLight(msg.body.state);
					else
						updateSlot(msg.body.name, msg.body.state);
				});
				init = true;
			}
		})
	}
}

function getCurrentDate()
{
	var today = new Date();
	var dd = today.getDate();
	var ddd = today.getDay();
	var mm = today.getMonth() + 1;

	var yyyy = today.getFullYear();
	if(dd < 10)
		dd = '0' + dd;
	if(mm < 10)
		mm = '0' + mm;
	switch (ddd)
	{
		case 0: ddd = "Sunday";
				break;
		case 1: ddd = "Monday";
				break;
		case 2: ddd = "Tuesday";
				break;
		case 3: ddd = "Wednesday";
				break;
		case 4: ddd = "Thursday";
				break;
		case 5: ddd = "Friday";
				break;
		case 6: ddd = "Saturday";
				break;
	}
	var today = ddd + ", " + dd + '/' + mm + '/' + yyyy;
	
	document.getElementById("date").innerHTML = today;
}

function updateFreeSlots()
{
	var cont = 0;
	
	for(var i = 0; i < parkState.length; i++)
		if (parkState[i])
			cont++;
		
	document.getElementById("freeSlots").innerHTML = cont;
}

function updateSlot(name, state)
{
	var index = parkName.indexOf(name);
	parkState[index] = state;
	
	if (state)
	{
		document.getElementById("state" + name).src = "img/free.png";
		document.getElementById("car" + name).style.visibility = "hidden";
	}
	else
	{
		document.getElementById("state" + name).src = "img/busy.png";
		document.getElementById("car" + name).style.visibility = "visible";			
	}
	
	updateFreeSlots();
}

function updateLight(state)
{
	var lights = document.getElementsByClassName("light");
	for (i = 0; i < lights.length; i++)
		if (state)
			lights[i].src = "img/light_on.png";
		else
			lights[i].src = "img/light_off.png";
}

function updateTime()
{
	var d = new Date();
	var t = d.toLocaleTimeString().substring(0,5);
	if (time != t)
    {
		time = t;
		document.getElementById("time").innerHTML = time;
	}
}