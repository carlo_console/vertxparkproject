package io.vertx.utils;

import java.io.IOException;
import com.pi4j.io.i2c.I2CBus;
import io.vertx.utils.ParkMap.SlotName;

// Classe utilizzata per gestire l'aggiornamento dell'LCD fisico
public class LcdManager
{
	private final int LCD_BUS_NO = I2CBus.BUS_1;
    private final int LCD_BUS_ADDRESS = 0x27;
    private LiquidCrystal_I2C lcd;
    
    public LcdManager() throws IOException
    {
    	System.out.println("Initializing LCD...");
    	lcd = new LiquidCrystal_I2C(LCD_BUS_NO, LCD_BUS_ADDRESS, 20, 4);
    	lcd.setCursor(0,0);
        lcd.print(" A1: Free  B1: Free");
        lcd.setCursor(0,1);
        lcd.print(" A2: Free  B2: Free");
        lcd.setCursor(0,2);
        lcd.print(" A3: Free  B3: Free");
        lcd.setCursor(0,3);
        lcd.print(" A4: Free  B4: Free");
        System.out.println("LCD ready.");
    }
    
	// Metodo che aggiorna lo stato del parcheggio richiesto (place)
    public void update(SlotName place, boolean state) throws IOException
    {
    	switch (place)
    	{
    		case A1:	
    			lcd.setCursor(5, 0);
    			break;
    		case A2:	
    			lcd.setCursor(5, 1);
    			break;
    		case A3:	
    			lcd.setCursor(5, 2);
    			break;
    		case A4:	
    			lcd.setCursor(5, 3);
    			break;
    		case B1:
    			lcd.setCursor(15, 0);
    			break;
    		case B2:
    			lcd.setCursor(15, 1);
    			break;
    		case B3:
    			lcd.setCursor(15, 2);
    			break;
    		case B4:
    			lcd.setCursor(15, 3);
    			break;
    		default: 
    			break;
    	}
    	
    	if (state)
    		lcd.print("Free");
    	else
    		lcd.print("Busy");
    }
}