package io.vertx.utils;

import io.vertx.core.json.JsonObject;

// Classe utilizzata per memorizzare lo stato complessivo del parcheggio
// (in particolare lo stato degli otto parcheggi e del sistema di illuminazione)
public class ParkMap
{
	private static boolean A1;
	private static boolean A2;
	private static boolean A3;
	private static boolean A4;
	
	private static boolean B1;
	private static boolean B2;
	private static boolean B3;
	private static boolean B4;
	
	private static boolean light;
	
	public static enum SlotName
	{
		A1, A2, A3, A4, B1, B2, B3, B4, LIGHT
	};
	
	// Metodo che aggiorna lo stato della proprietà del parcheggio richiesta (name)
	public static void setSingleState(SlotName name, boolean state)
	{
		switch (name)
		{
			case A1:
				A1 = state;
				break;
			case A2:
				A2 = state;
				break;
			case A3:
				A3 = state;
				break;
			case A4:
				A4 = state;
				break;
			case B1:
				B1 = state;
				break;
			case B2:
				B2 = state;
				break;
			case B3:
				B3 = state;
				break;
			case B4:
				B4 = state;
				break;
			case LIGHT:
				light = state;
				break;
		}
	}
	
	// Metodo che restituisce lo stato complessivo del parcheggio sotto forma di oggetto JSON
	// (utilizzato per inizializzare le GUI dei client)
	public static JsonObject getParkStateAsJson()
	{
		return new JsonObject().put("A1", A1).put("A2", A2).put("A3", A3).put("A4", A4)
							   .put("B1", B1).put("B2", B2).put("B3", B3).put("B4", B4)
							   .put("LIGHT", light);
	}
	
	public static SlotName toSlotName(String name)
	{
		switch (name)
		{
			case "A1":
				return SlotName.A1;
			case "A2":
				return SlotName.A2;
			case "A3":
				return SlotName.A3;
			case "A4":
				return SlotName.A4;
			case "B1":
				return SlotName.B1;
			case "B2":
				return SlotName.B2;
			case "B3":
				return SlotName.B3;
			case "B4":
				return SlotName.B4;
			case "LIGHT":
				return SlotName.LIGHT;
			default: 
				return null;
		}
	}
}