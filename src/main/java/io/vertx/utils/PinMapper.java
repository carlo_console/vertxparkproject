package io.vertx.utils;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

// Classe che mappa i pin utilizzati dei sensori con dei nomi User Friendly
public class PinMapper
{
	private final static Pin TRIGA1 = RaspiPin.GPIO_21;
	private final static Pin TRIGA2 = RaspiPin.GPIO_11;
	private final static Pin TRIGA3 = RaspiPin.GPIO_10;
	private final static Pin TRIGA4 = RaspiPin.GPIO_06;
	private final static Pin TRIGB1 = RaspiPin.GPIO_14;
	private final static Pin TRIGB2 = RaspiPin.GPIO_13;
	private final static Pin TRIGB3 = RaspiPin.GPIO_12;
	private final static Pin TRIGB4 = RaspiPin.GPIO_05;
	
	private final static Pin ECHOA1 = RaspiPin.GPIO_15;
	private final static Pin ECHOA2 = RaspiPin.GPIO_16;
	private final static Pin ECHOA3 = RaspiPin.GPIO_01;
	private final static Pin ECHOA4 = RaspiPin.GPIO_04;
	private final static Pin ECHOB1 = RaspiPin.GPIO_07;
	private final static Pin ECHOB2 = RaspiPin.GPIO_00;
	private final static Pin ECHOB3 = RaspiPin.GPIO_02;
	private final static Pin ECHOB4 = RaspiPin.GPIO_03;
	
	private final static Pin LIGHTPIN = RaspiPin.GPIO_22;
	private final static Pin LEDPIN = RaspiPin.GPIO_26;
	private final static Pin PIRPIN = RaspiPin.GPIO_23;
	
	public static enum PinValue
	{
		TRIGA1, TRIGA2, TRIGA3, TRIGA4, TRIGB1, TRIGB2, TRIGB3, TRIGB4,
		ECHOA1, ECHOA2, ECHOA3, ECHOA4, ECHOB1, ECHOB2, ECHOB3, ECHOB4,
		LIGHTPIN, LEDPIN, PIRPIN
	};
	
	// Metodo che restituisce il pin corrispondente al nome richiesto
	public static Pin getPinByName(PinValue pin)
	{	
		switch (pin)
		{
			case TRIGA1:
				return TRIGA1;
			case TRIGA2:
				return TRIGA2;
			case TRIGA3:
				return TRIGA3;
			case TRIGA4:
				return TRIGA4;
			case TRIGB1:
				return TRIGB1;
			case TRIGB2:
				return TRIGB2;
			case TRIGB3:
				return TRIGB3;
			case TRIGB4:
				return TRIGB4;
				
			case ECHOA1:
				return ECHOA1;
			case ECHOA2:
				return ECHOA2;
			case ECHOA3:
				return ECHOA3;
			case ECHOA4:
				return ECHOA4;
			case ECHOB1:
				return ECHOB1;
			case ECHOB2:
				return ECHOB2;
			case ECHOB3:
				return ECHOB3;
			case ECHOB4:
				return ECHOB4;
				
			case LIGHTPIN:
				return LIGHTPIN;
			case LEDPIN:
				return LEDPIN;
			case PIRPIN:
				return PIRPIN;

			default:
				return null;
		}
	}
}
