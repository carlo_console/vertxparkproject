package io.vertx.parking;

import java.io.IOException;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeEventType;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.utils.LcdManager;
import io.vertx.utils.ParkMap;
import io.vertx.utils.ParkMap.SlotName;
import io.vertx.utils.PinMapper.PinValue;
import io.vertx.utils.Runner;

// L'estensione AbstractVerticle è indispensabile per l'utilizzo dell'istanza di Vertx
public class Server extends AbstractVerticle
{
	private final static int DEPLOY_DELAY = 70;
	private LcdManager lcd;
	
	// Clase che permette di lanciare il programma direttamente dall'IDE
	public static void main(String[] args)
	{
		Runner.runExample(Server.class);
	}

	@Override
	public void start() throws Exception
	{
		// Trascorsi 200ms dall'avvio, viene inizializzato l'LCD fisico
		vertx.setTimer(200, h -> {
				try 
				{
					lcd = new LcdManager();
				} catch (IOException e1) { System.out.println("Errore LCD!");}
		});
		// Oggetto che gestisce tutte le richieste in arrivo al server
		Router router = Router.router(vertx);
		
	    // Opzioni del router che permettono ai client di collegarsi agli indirizzi "news-feed" e "init" dell'Event-Bus.
		// In questo modo i due indirizzi diventano distribuiti
	    BridgeOptions options = new BridgeOptions().addOutboundPermitted(new PermittedOptions().setAddress("news-feed"))
	    										   .addOutboundPermitted(new PermittedOptions().setAddress("init"));
												   
		// Tutte le richieste client indirizzate alla risorsa "/eventbus/*" vengono gestite con la creazione di una sockJS, 
		// caratterizzata dalle opzioni definite precedentemente. Viene utilizzata una sockJS in quanto le richieste 
		// provengono da un file javascript (sockJS è lo standard che implementa le webSocket in questo linguaggio)
	    router.route("/eventbus/*").handler(SockJSHandler.create(vertx).bridge(options, event -> {
	    	// Ogni volta che viene effettuata una richiesta, viene pubblicato un messaggio sull'indirizzo "init"
			// per inviare lo stato complessivo del parcheggio al client che si è appena connesso
			if (event.type() == BridgeEventType.SOCKET_CREATED)
	    	{
	    		System.out.println("A socket was created");
	    		vertx.setTimer(1000, h -> vertx.eventBus().publish("init", ParkMap.getParkStateAsJson()));	    		
	    	}
			// La richiesta è stata processata con successo
	    	event.complete(true); })
	    );
	    
	    // Viene utilizzata la cartella di default "webroot" per fornire le risorse statiche (html, css, javascript, ...)
	    router.route().handler(StaticHandler.create());
	    
		// Viene avviato l'http server, che accetta le richieste definite dall'oggetto router, 
		// messo in ascolto sulla porta 8080 dell'indirizzo locale
	    for(int i = 0; i < 4; i++)
		    vertx
		    .createHttpServer()
		    .requestHandler(router::accept)
		    .listen(8080);

		// Il Server Verticle si sottoscrive all'indirizzo "sensor" dell'EventBus, per ricevere continuamente i dati
		// pubblicati dai vari sensori. Ogni volta che viene ricevuto un dato:
		// - Se si tratta di un dato relativo ai parcheggi, viene aggiornato l'LCD;
		// - Viene aggiornata la classe statica ParkMap con il nuovo stato ricevuto; 
		// - Viene inoltrato il cambiamento sul canale pubblico "news-feed", in modo da aggiornare l'interfaccia grafica
		//   di tutti i client connessi
	    vertx.eventBus().consumer("sensor", ev -> {
	    	JsonObject obj = (JsonObject) ev.body();
	    		    	
			if(obj.getString("name").equals(SlotName.LIGHT.toString()) == false)
				try
				{
					lcd.update(ParkMap.toSlotName(obj.getString("name")), obj.getBoolean("state"));
				} catch (IOException e) { }
	    	ParkMap.setSingleState(ParkMap.toSlotName(obj.getString("name")), obj.getBoolean("state"));
	    	
	    	vertx.eventBus().publish("news-feed", ev.body());
	    });
	    
		// Dopo 1,5s vengono istanziati tutti i Verticle Worker:
		// - 8 per i sensori di distanza;
		// - 1 per il sistema di illuminazione;
		// - 1 per il sistema di accesso al parcheggio
		// Per istanziare un Worker è necessario fornire la classe che lo implementa (DistanceSensor, LightSensor, PIRSensor)
		// e fornirgli delle configurazioni opzionali, come ad esempio i pin utilizzati, il nome del sensore, ...
	    vertx.setTimer(1500, h -> {
	    	try
	    	{
		    	vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGA1).put("echoPin", PinValue.ECHOA1).put("name", SlotName.A1)));
		    	
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGA2).put("echoPin", PinValue.ECHOA2).put("name", SlotName.A2)));
		    	
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGA3).put("echoPin", PinValue.ECHOA3).put("name", SlotName.A3)));
			    
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGA4).put("echoPin", PinValue.ECHOA4).put("name", SlotName.A4)));
			    
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGB1).put("echoPin", PinValue.ECHOB1).put("name", SlotName.B1)));
			    
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGB2).put("echoPin", PinValue.ECHOB2).put("name", SlotName.B2)));
			    
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGB3).put("echoPin", PinValue.ECHOB3).put("name", SlotName.B3)));
			    
		    	Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.DistanceSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("trigPin", PinValue.TRIGB4).put("echoPin", PinValue.ECHOB4).put("name", SlotName.B4)));
	    	
			    Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.LightSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("lightPin", PinValue.LIGHTPIN).put("ledPin", PinValue.LEDPIN).put("name", SlotName.LIGHT)));
			    
			    Thread.sleep(DEPLOY_DELAY);
			    vertx.deployVerticle("io.vertx.parking.PIRSensor", new DeploymentOptions().setWorker(true).setConfig(new JsonObject().put("pirPin", PinValue.PIRPIN)));
	    	}
		    catch (Exception e) { };
	    });
	}
}