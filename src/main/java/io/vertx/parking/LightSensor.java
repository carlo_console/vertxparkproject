package io.vertx.parking;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalMultipurpose;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinMode;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.utils.PinMapper;
import io.vertx.utils.PinMapper.PinValue;

// L'estensione AbstractVerticle è indispensabile per l'utilizzo dell'istanza di Vertx
public class LightSensor extends AbstractVerticle
{
	// Soglia oltre la quale va acceso il sistema di illuminazione e sotto la quale va spento
	private final static long LUX_BOUND = 1000;
	private static GpioController gpio;
	private static GpioPinDigitalMultipurpose light;
	private static GpioPinDigitalOutput led;
	private long lux;
	
	// Questo Worker ha il compito di controllare periodicamente la luminosità dell'ambiente:
	// - se risulta scarsa, accende il sistema di illuminazione e notifica il cambiamento al server tramite EventBus 
	// - se risulta elevata, spegne il sistema di illuminazione e notifica il cambiamento al server tramite EventBus
	@Override
	public void start() throws InterruptedException 
	{
		// Inizializzazione dei componenti fisici
		gpio = GpioFactory.getInstance();
		led = gpio.provisionDigitalOutputPin((PinMapper.getPinByName(PinValue.valueOf(config().getString("ledPin")))));
		light = gpio.provisionDigitalMultipurposePin((PinMapper.getPinByName(PinValue.valueOf(config().getString("lightPin")))), PinMode.DIGITAL_INPUT);
		led.low();
		
		lightCheck();
		// Periodicamente, viene letta la luminosità dell'ambiente ed effettuato il controllo descritto precedentemente
		vertx.setPeriodic(100, h -> {
			try 
			{
				lightCheck();
			} catch (InterruptedException e) { };
		});
	}
	
	// Metodo che permette di leggere la luminosità dell'ambiente, espresso in cicli di carica del condensatore:
	// - numero di cicli alto -> resistenza alta -> luminosità bassa
	// - numero di cicli basso -> resistenza bassa -> luminosità alta
	private long readLux() throws InterruptedException
	{
		long lux = 0;
		
		light.setMode(PinMode.DIGITAL_OUTPUT);
		light.low();
		Thread.sleep(100);
		light.setMode(PinMode.DIGITAL_INPUT);
		while (light.isLow())
		  lux ++;

		return lux;
	}
	
	// Metodo che legge la luminosità dell'ambiente e accende/spegne il sistema di illuminazione in base alle necessità,
	// notificando inoltre il cambiamento al server
	private void lightCheck() throws InterruptedException
	{
		lux = readLux();
		if (lux >= LUX_BOUND && led.isLow())
		{
			led.high();
			vertx.eventBus().publish("sensor", new JsonObject().put("name", config().getString("name")).put("state",true));
		}
		else if (lux < LUX_BOUND && led.isHigh())
		{
			led.low();
			vertx.eventBus().publish("sensor", new JsonObject().put("name", config().getString("name")).put("state",false));
		}
	}
}