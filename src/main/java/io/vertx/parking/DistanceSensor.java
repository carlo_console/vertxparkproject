package io.vertx.parking;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.utils.PinMapper;
import io.vertx.utils.PinMapper.PinValue;

// L'estensione AbstractVerticle � indispensabile per l'utilizzo dell'istanza di Vertx
public class DistanceSensor extends AbstractVerticle
{
	// Velocit� del suono in m/s
	private final static float SOUND_SPEED = 340.29f;
	// Durata di attivazione del trigger (10 micro s)
    private final static int TRIG_DURATION_IN_MICROS = 10;
    // Tempo di attesa massimo che il sensore aspetta per ricevere il segnale di ritorno
    private final static int TIMEOUT = 6500;
    // Soglia (in cm) che distingue un parcheggio da libero a occupato
    private final static int MIN_DISTANCE = 10;
    // Costante utilizzata per evitare letture errate dei sensori (una lettura viene ritenuta attendibile
    // se rimane coerente per ERR_BOUND volte)
    private final static int ERR_BOUND = 5;
    // Stato del parcheggio (true: libero, false: occupato)
    private boolean state;
    // Contatore utilizzato per evitare letture errate dei sensori
    private int errCheck = 0;
    private final static GpioController gpio = GpioFactory.getInstance();
    private GpioPinDigitalInput echoPin;
    private GpioPinDigitalOutput trigPin;
    
	// Questo Worker � associato ad un unico posto auto e il suo compito � quello di controllare periodicamente lo stato del parcheggio,
	// e comunicarne eventualmente il cambiamento
	@Override
	public void start() throws Exception 
	{
		// Inizializzazione dei componenti fisici
		this.echoPin = gpio.provisionDigitalInputPin(PinMapper.getPinByName(PinValue.valueOf(config().getString("echoPin"))));
        this.trigPin = gpio.provisionDigitalOutputPin(PinMapper.getPinByName(PinValue.valueOf(config().getString("trigPin"))));
        this.trigPin.low();
        
        // Viene comunicata la prima lettura del sensore per inizializzare il posto del parcheggio
		if (measureDistance() < MIN_DISTANCE)
			state = false;
		else
			state = true;
		vertx.eventBus().publish("sensor", new JsonObject().put("name", config().getString("name")).put("state", state));
		
		// Ogni 200ms viene controllato lo stato del parcheggio. Ogni volta che viene identificato un cambiamento, viene
		// controllato per tre volte, per evitare false letture del sensore. Se non si tratta di false letture, viene
		// effettivamente pubblicato un messaggio, contenente l'id del sensore e il nuovo stato letto
        vertx.setPeriodic(200, t -> {
			try
			{
				float dist = measureDistance();
				// Se lo stato del parcheggio � libero e si legge per 'ERR_BOUND' volte un cambiamento di stato,
				// il parcheggio viene segnato come occupato e viene generato l'evento del cambiamento
				if (state)
				{
					if (dist <= MIN_DISTANCE)
					{
						if (errCheck == ERR_BOUND)
						{
							state = false;
							errCheck = 0;
							vertx.eventBus().publish("sensor", new JsonObject().put("name", config().getString("name")).put("state", state));
						}
						else
							errCheck++;
					}
					else
						errCheck = 0;
				}
				// Se lo stato del parcheggio � occupato e si legge per 'ERR_BOUND' volte un cambiamento di stato,
				// il parcheggio viene segnato come libero e viene generato l'evento del cambiamento
				else
				{
					if (dist > MIN_DISTANCE)
					{
						if (errCheck == ERR_BOUND)
						{
							state = true;
							errCheck = 0;
							vertx.eventBus().publish("sensor", new JsonObject().put("name", config().getString("name")).put("state", state));
						}
						else
							errCheck++;
					}
					else
						errCheck = 0;
				}
			} catch (Exception e) { }
		});
	}
    
    // Metodo che restituisce la distanza in cm letta dal sensore
    public float measureDistance() throws Exception
    {
        this.triggerSensor();
        this.waitForSignal();
        long duration = this.measureSignal();
        
        return duration * SOUND_SPEED / ( 2 * 10000 );
    }

    // Metodo che inizializza il sensore HC-SR04 con un segnale alto di 10us
    private void triggerSensor()
    {
        try
        {
            this.trigPin.high();
            Thread.sleep( 0, TRIG_DURATION_IN_MICROS * 1000 );
            this.trigPin.low();
        } 
        catch (InterruptedException ex)
        {
            System.err.println( "Interrupt during trigger" );
        }
    }
    
    // Metodo che attende che il sensore invii il segnale di partenza
    private void waitForSignal() throws Exception
    {
        int countdown = TIMEOUT;
        
        while(this.echoPin.isLow() && countdown > 0)
            countdown--;
        if( countdown <= 0 )
            throw new Exception("Timeout waiting for signal start");
    }
    
    // Metodo che attende il segnale di ritorno e restituisce la durata in us
    private long measureSignal() throws Exception
    {
        int countdown = TIMEOUT;
        long start = System.nanoTime();
        while(this.echoPin.isHigh() && countdown > 0)
            countdown--;
        
        long end = System.nanoTime();
        
        if(countdown <= 0)
            throw new Exception("Timeout waiting for signal end");
        
        return (long)Math.ceil((end - start)/ 1000.0);
    }
}