package io.vertx.parking;

import java.util.concurrent.Callable;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.pi4j.wiringpi.SoftPwm;
import io.vertx.core.AbstractVerticle;
import io.vertx.utils.PinMapper;
import io.vertx.utils.PinMapper.PinValue;

// L'estensione AbstractVerticle è indispensabile per l'utilizzo dell'istanza di Vertx
public class PIRSensor extends AbstractVerticle
{
	// Tempo impiegato dalla sbarra per aprirsi/chiudersi
	private final static int BARRIER_WORKING_TIME = 1000;
	// Tempo concesso al veicolo per transitare
	private final static int BARRIER_IDLE_TIME = 8000;
	private Pin ENABLE_PIN = RaspiPin.GPIO_27;
	private Pin CW_PIN = RaspiPin.GPIO_28;
	private Pin CCW_PIN = RaspiPin.GPIO_29;
	private final static GpioController gpio = GpioFactory.getInstance();
	private GpioPinDigitalInput pir;
	private GpioPinDigitalOutput cw;
    private GpioPinDigitalOutput ccw;
	// Stato della barriera:
	// false: abbassata
	// true: alzata
    private boolean barrier = false;
	
	// Questo Worker ha il compito di gestire il sistema di accesso al parcheggio:
	// ogni volta che viene rilevato un movimento dal PIR, bisogna alzare la sbarra, mantenerla
	// alzata per un periodo di tempo e riabbassarla una volta che è transitata l'auto
	@Override
	public void start() throws Exception 
	{
		// Inizializzazione del componenti fisici (PIR, L293D, Input per l'apertura CCW e Input per la chiusura CW)
        pir = gpio.provisionDigitalInputPin((PinMapper.getPinByName(PinValue.valueOf(config().getString("pirPin")))));
        cw = gpio.provisionDigitalOutputPin(CW_PIN);
	    ccw = gpio.provisionDigitalOutputPin(CCW_PIN);
	    SoftPwm.softPwmCreate(ENABLE_PIN.getAddress(), 0, 100);
		
        // Callback che viene richiamata ogni volta che il PIR rileva un movimento
        Callable<Void> callback = () -> {
            System.out.println(" PIR Triggered (state: " + barrier + ")");
			if (barrier == false)
			{
				// Apertura barriera
				barrier = true;
				openBarrier();
				System.out.println("Barrier opening...");
				
				vertx.setTimer(BARRIER_WORKING_TIME, h1 -> { 
					// Barriera alzata, stato di attesa
					System.out.println("Barrier opened!");
					vertx.setTimer(BARRIER_IDLE_TIME, h2 -> { 
						// Chiusura barriera
						closeBarrier();
						System.out.println("Barrier closing...");
						vertx.setTimer(BARRIER_WORKING_TIME, h3 -> {
							// Barriera chiusa
							System.out.println("Barrier closed! (state: " + barrier + ")");
							barrier = false;
						});
					});
				});
			}
            return null;
        };
		
        // Viene agganciata la callback appena definita al pin del PIR, nel momento
		// in cui viene letto lo stato High
        pir.addTrigger(new GpioCallbackTrigger(PinState.HIGH, callback));
		
        vertx.setPeriodic(200, t -> { });
	}
	
	// Metodo per far girare in senso orario il motore a velocità massima
	private void openBarrier()
	{
		SoftPwm.softPwmWrite(ENABLE_PIN.getAddress(), 100);
    	ccw.high();
    	vertx.setTimer(215, h -> ccw.low());
	}
	
	// Metodo per far girare in senso antiorario il motore a velocità 60%
	private void closeBarrier()
	{
		SoftPwm.softPwmWrite(ENABLE_PIN.getAddress(), 60);
    	cw.high();
    	vertx.setTimer(195, h -> cw.low());
	}
}